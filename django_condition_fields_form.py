class SomeForm(forms.ModelForm):

    class Meta:
        model = SomeModel
        fields = '__all__'
        
    def clean(self):
        cleaned_data = super(SomeForm, self).clean()
        field1 = cleaned_data.get("field1")
        field2 = cleaned_data.get("field2")
        if field1 is None or field2 is None:
            raise forms.ValidationError("Необходимо заполнить оба поля")
        return cleaned_data