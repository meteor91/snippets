    try:
        box = work.social_image_crop.split(',')

        thumbnail_url = get_thumbnailer(work.social_image).get_thumbnail({
            'box': work.social_image_crop, 
            'crop': True, 
            'size':(int(box[2]) - int(box[0]), int(box[3]) - int(box[1]))
        })
    except:
        thumbnail_url = ''
    data = {
        'data': t.render(Context()),
        'case': 'OK',
        'has_scripts': work.case_has_scripts,
        'has_styles': work.case_has_styles,
        'name': work.name_big,
        'social_title': work.social_title,
        'social_description': work.social_description,
        'social_image': thumbnail_url

    }