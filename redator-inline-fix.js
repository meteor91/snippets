(function ($) {
    $(document).on('click', '.add-row', function () {
        $('.inline-related:not(.empty-form)')
            .eq(-2)
            .find('textarea.redactor-box')
            .trigger('redactor:init');
    });
})(django.jQuery);